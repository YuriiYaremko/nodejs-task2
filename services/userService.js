const { User } = require('../models/userModel');

const bcrypt = require('bcrypt');

const getUserInfo = async (userId) => {
  const user = await User.findById(userId);
  return user;
}

const deleteUser = async (userId) => {
  await User.findOneAndRemove({ _id: userId });
}

const changeUserPassword = async (userId, newPassword, oldPassword) => {
  const user = await User.findOne({ _id: userId });
  const compared = await bcrypt.compare(oldPassword, user.password);

  if (!compared) {
    return false
  }

  await User.findOneAndUpdate({ _id: userId }, { password: await bcrypt.hash(newPassword, 10) });
  return true;
};

module.exports = {
  getUserInfo,
  deleteUser,
  changeUserPassword
};