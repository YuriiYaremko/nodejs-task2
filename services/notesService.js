const { Note } = require('../models/noteModel');

const getNotesById = async (userId, offset = 0, limit = 0) => {
    const notes = await Note.find({ userId })
        .skip(Number(offset))
        .limit(Number(limit));
    return {
        offset,
        limit,
        count: notes.length,
        notes: notes
    };
}

const getNoteByIdForUser = async (id, userId) => {
    const note = await Note.findOne({ _id: id, userId });
    return note;
}


const addNoteToUser = async (userId, notePayload) => {
    const note = new Note({ ...notePayload, userId });
    await note.save();
}

const updateNote = async (id, userId, dattexta) => {
    return await Note.findOneAndUpdate({ _id: id, userId }, { $set: { text } });
}

const changeToCompleted = async (noteId, userId, data) => {
    const completed = note.completed ? false : true;
    await Note.findOneAndUpdate({ _id: id, userId }, { completed });
}

const deleteNote = async (noteId, userId) => {
    await Note.findOneAndRemove({ _id: noteId, userId });
}

module.exports = {
    getNotesById,
    getNoteByIdForUser,
    addNoteToUser,
    updateNote,
    changeToCompleted,
    deleteNote
};