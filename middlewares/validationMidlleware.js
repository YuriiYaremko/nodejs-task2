const Joi = require('joi');

const regValidator = async (req, res, next) => {
    const schema = Joi.object({
        username: Joi.string()
            .alphanum()
            .required(),
        password: Joi.string()
            .min(5)
            .max(20)
            .required(),
    });

    try {
        await schema.validateAsync(req.body);
        next();
    }
    catch (err) {
        next(err);
    }
};

module.exports = {
    regValidator,
}