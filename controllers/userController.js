const express = require('express');

const router = new express.Router();

const {
  getUserInfo,
  changeUserPassword,
  deleteUser,
} = require('../services/userService');

const {
  asyncWrapper
} = require('../utils/apiUtils');
const {
  InvalidRequestError
} = require('../utils/errors');

router.get('/', asyncWrapper(async (req, res) => {
  const { userId } = req.user;

  const user = await getUserInfo(userId);
  if (!user) {
    throw new InvalidRequestError('No user with such id found!');
  }
  res.json({ user });
}));

router.delete('/', asyncWrapper(async (req, res) => {
  const { userId } = req.user;

  await deleteUser(userId);

  res.json({ message: "User deleted successfully" });
}));

router.patch('/', asyncWrapper(async (req, res) => {
  const { userId } = req.user;
  const { oldPassword, newPassword } = req.body;

  await changeUserPassword(userId, newPassword, oldPassword);

  res.json({ message: "Password updated successfully" });
}));


module.exports = {
  userRouter: router
}